#Rule ID to Rule Path Util
This util is use to create a mapping file between ruleId to RulePath.

###Step to create the mapping file:
 1. Get the latest rules response from the server by calling `https://<YOUR-SERVER>/api/v1/rules` end point
 2. Extract the rule id from the JSON response in to a file. `ruleId.txt` file:
    * You can use [jq](https://stedolan.github.io/jq/): `brew install jq`
    * Run `jq '.[].id' rule.json | uniq > ruleId.txt` were `ruleapi.json` is the saved response from step 1
 3. Run `index.js` to generate the ruleIdToRulePath.json file. You will need to provide the `ruleId.txt` file and the rule directory

