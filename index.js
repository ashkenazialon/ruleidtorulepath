var findInFiles = require('find-in-files');
var lineReader = require('line-reader');
var fs = require('fs');


var fileName = "ruleIdToRulePath.json";
var ruleIdFile = "/Users/alonashkenazi/Desktop/ruleIds.txt";
var ruleLib = '/Users/alonashkenazi/repos/indeni/server/server-core/src/main/scala/com/indeni/server/rules/library/';

fs.writeFileSync(fileName, "{");
lineReader.eachLine(ruleIdFile, function(line, last) {
    var lineAppend = last ? "}" : ",";

    findInFiles.find(line, ruleLib, '.scala')
        .then(function(results) {
            for (var result in results) {
                var res = results[result];
                fs.appendFileSync(fileName, res.matches[0] + ':"' + result.replace(ruleLib,"") + '"' + lineAppend);
                console.log(res.matches[0] + ':"' + result.replace(ruleLib,"") + '",');
            }
        });
});





